from django.urls import path
from finalapp import views

app_name = 'finalapp'
urlpatterns = [

        path('',views.dashboard.as_view(),name='profile'),
        path('logout/',views.logout,name='logout'),

        path('list/',views.PostListView.as_view(),name='list'),
        path('<int:pk>/',views.PostDetailView.as_view(),name='post_detail'),
        path('post/create/',views.PostCreateView.as_view(),name='create'),
        path('post/update/<int:pk>',views.PostUpdateView.as_view(),name='post_update'),
        path('post/delete/<int:pk>',views.PostDeleteView.as_view(),name='post_delete'),

        path('create/',views.registerUpdateView.as_view(),name='user_create'),
        path('change/password',views.updatePass,name='updatePass'),
        path('update/<int:pk>',views.registerUpdateView.as_view(),name='user_update'),
        path('user/friendz/',views.friends,name='friends'),
        path('user_inbox/',views.user_inbox,name='user_inbox'),
        path('user/messages',views.message_list.as_view(),name='message_list'),

        path('user/ajax/message/',views.save_message_ajax,name='ajax_message'),
        path('user/inbox/all/',views.show_message_all,name='show_message'),
        path('user/name/ajax',views.ajax_fun,name='ajax_fun'),
        path('user/profile',views.userProfile,name='user_profile'),
        path('user/newsfeed',views.news_feed,name='news_feed'),
        path('user/feed/description',views.feed_description, name='feed_description'),




]

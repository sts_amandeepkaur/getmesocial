from django.contrib import admin
from finalapp.models import register, Message, Post,followUsers,inbox, postcomment,likes

admin.site.site_header = "GetMeSocial | Admin"

class registerAdmin(admin.ModelAdmin):
    list_display =['id','user','user_id','date_of_birth','gender','current_city','hometown','education_details','interested_in','marital_status','worked_at','about_you']

class followUsersAdmin(admin.ModelAdmin):
    list_display =['id','follower','following','is_friend']
    search_fields =['follower']

class PostAdmin(admin.ModelAdmin):
    list_display =['id','author','title','text']

class MessageAdmin(admin.ModelAdmin):
    list_display =['id','name','email','message']

class inboxAdmin(admin.ModelAdmin):
    list_display =['id','sender','receiver','message']

class postcommentAdmin(admin.ModelAdmin):
    list_display =['id','postid','commenterid','comment']

class likesAdmin(admin.ModelAdmin):
    list_display =['id','follower','postid','is_like']

admin.site.register(register,registerAdmin)
admin.site.register(Message,MessageAdmin)
admin.site.register(Post,PostAdmin)
admin.site.register(followUsers,followUsersAdmin)
admin.site.register(inbox,inboxAdmin)
admin.site.register(postcomment,postcommentAdmin)
admin.site.register(likes,likesAdmin)

from django.db import models
from django.db import models
from django.contrib.auth.models import User
# Create your models here.
from django.utils import timezone
from django.urls import reverse
class register(models.Model):
    GENDER = [('male','male'),
            ('female','female')]

    M_STATUS = [('Married ','Married '),
            ('engased ','engased '),
            ('commited ','commited '),
            ('Widowed ','Widowed '),
            ('Separated ','Separated '),
            ('Divorced ','Divorced '),
            ('Single ','Single '),
            ("It's complicated","It's complicated"),
            ("other","other"),]

    INTERESTS= [('men','men'),
            ('women','women'),
            ('both','both')]

    user = models.OneToOneField(User,on_delete=models.CASCADE)
    date_of_birth= models.DateField(null=True)
    gender = models.CharField(choices=GENDER,max_length = 200,default='female')
    profile_pic = models.FileField(null=True,blank=True, upload_to = 'profile/%Y/%m/%d')
    # Additional Fields
    current_city = models.CharField(null=True,max_length=250,blank=True)
    hometown = models.CharField(null=True,max_length=250,blank=True)
    education_details =models.CharField(null=True,max_length=250,blank=True)
    interested_in = models.CharField(choices=INTERESTS,null=True,max_length=250,blank=True)
    marital_status = models.CharField(choices=M_STATUS, null=True,max_length=250,blank=True)
    worked_at = models.CharField(null=True,max_length=250,blank=True)
    about_you = models.TextField(blank=True, null=True)
    registered_at = models.DateTimeField(auto_now_add = True,null=True)


    def __str__(self):
        return self.user.username
    # def get_absolute_url(self):
    #     return reverse('user_update', kwargs={'pk': self.pk})

class Post(models.Model):
    author = models.ForeignKey(User,on_delete= models.CASCADE)
    title = models.CharField(max_length=250)
    image = models.FileField(null=True, upload_to = 'post/%Y/%m/%d')
    text = models.TextField()
    create_date = models.DateTimeField(default=timezone.now())
    published_date = models.DateTimeField(blank = True,null = True)
    comments = models.IntegerField(null=True)
    likes =models.IntegerField(null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()
    def get_absolute_url(self):
        return reverse('finalapp:post_detail',kwargs={'pk':self.pk})


    def __str__(self):
        return self.title


class Message(models.Model):
    name=models.CharField(max_length=20)
    email=models.EmailField(max_length=50)
    message=models.CharField(max_length=20)

    class Meta:
        verbose_name_plural = "Contact Us"

class followUsers(models.Model):
    follower = models.IntegerField()
    following = models.IntegerField()
    create_date = models.DateTimeField(auto_now_add = True)
    is_friend = models.BooleanField(default=False)

    def __str__(self):
        return repr(self.follower)

    class Meta():
        verbose_name_plural="Follower Table"

class inbox(models.Model):
    sender=models.IntegerField()
    receiver = models.IntegerField()
    message=models.TextField()
    status=models.BooleanField(default=False)
    approved =models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.message

class postcomment(models.Model):
    postid = models.IntegerField()
    commenterid = models.ForeignKey(register,on_delete=models.CASCADE,null=True)
    comment = models.TextField()
    create_date = models.DateTimeField(auto_now_add = True)
    aproved= models.BooleanField(default=False)

    def __str__(self):
        return repr(self.postid)

class likes(models.Model):
    follower = models.IntegerField()
    postid = models.IntegerField()
    create_date = models.DateTimeField(auto_now_add = True)
    is_like = models.BooleanField(default=False)

    def __str__(self):
        return repr(self.follower)

    class Meta():
        verbose_name_plural="Like Table"
